
import numpy as np
import random
from collections import deque
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
class DQNSolver:
    def __init__(self, observation_space, action_space, exp):
        EXPLORATION_MAX = exp
        MEMORY_SIZE = 10000
        LEARNING_RATE = 0.01
        self.exploration_rate = EXPLORATION_MAX
        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)
        self.model = Sequential()
        self.model.add(Dense(100, input_shape=(observation_space,), activation="relu"))
        self.model.add(Dense(200, activation="relu"))
        self.model.add(Dense(200, activation="relu"))
        self.model.add(Dense(200, activation="relu"))
        self.model.add(Dense(200, activation="relu"))
        self.model.add(Dense(self.action_space, activation="linear"))
        self.model.compile(loss="mse", optimizer=Adam(lr=LEARNING_RATE))

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def load(self, model_name):
        self.model = tf.keras.models.load_model(model_name)
        self.model.compile(loss="mse", optimizer=Adam(lr = 0.01))
    
    def save(self, model_name):
        self.model.save(model_name)

    def act(self, state, training):
        if np.random.rand() < self.exploration_rate and training:
            return random.randrange(self.action_space)
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def experience_replay(self):
        BATCH_SIZE = 5000
        GAMMA = 0.95
        EXPLORATION_DECAY = 0.95
        EXPLORATION_MIN = 0.05
        if len(self.memory) < BATCH_SIZE:
            return
        else:
            print("Experiencing the world")
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, state_next, terminal in batch:
            q_update = reward
            if not terminal:
                q_update = (reward + GAMMA * np.amax(self.model.predict(state_next)[0]))
            q_values = self.model.predict(state)
            q_values[0][action] = q_update
            self.model.fit(state, q_values, verbose=0)
        self.exploration_rate *= EXPLORATION_DECAY
        self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)
