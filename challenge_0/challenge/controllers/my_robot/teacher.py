import os
import sys
import signal
from subprocess import Popen, PIPE, STDOUT
import time

webots_path = "/home/visakoe1/installations/webots/webots"
world_path = "/home/visakoe1/ohjelmointi/artificialinvaders2019/challenge_0/challenge/arena/challenge0.wbt"
seconds_per_epoch = 6

proc = None
while True:
    try:
        print("New epoch, new kujeet")
        proc = Popen(
            [webots_path, "--batch", '--mode=fast', world_path],
            stdout=PIPE,
            stdin=PIPE,
            stderr=STDOUT,
            preexec_fn=os.setsid,
        )
        time.sleep(seconds_per_epoch)
    finally:
        os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
