# EDIT THIS FILE

# InvaderBot is a simple helper class which has methods to control the robot.
# It offers an interface over Webots' default motor controller.
# You can use it, modify it or you can also use Webots' own commands.
# Using the helper class is recommended because it will later help you to
# transfer your code into a physical robot, which is running on Raspberry Pi.
import numpy as np
import os
from invader_bot import InvaderBot
from model import DQNSolver
from reward import rewardf
from featurespace import features

model_name = './simplemodel.h5'
bot = InvaderBot()
bot.setup()
training = True
fspace = 17
brain = DQNSolver(fspace, 2, 1)
last_state = np.arange(fspace)
last_state.fill(-100)
last_state = last_state.reshape(1, fspace)

if os.path.exists(model_name):
    brain.load(model_name) #load old model if exists
    print('Found old model')

def turn(bot, bin):
    bot.set_motors(bin,1 - bin)

print("start")
try:
    while bot.step():        
        # get current time
        time = bot.get_time()
        state = features(bot,fspace).reshape(1,-1)
        action = brain.act(state, training)
        turn(bot, action)
        brain.remember(last_state, action, rewardf(bot), state, False)
        last_state = state
        brain.experience_replay()
finally:
    brain.save(model_name)
