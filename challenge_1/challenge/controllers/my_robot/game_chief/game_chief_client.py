import grpc
from concurrent import futures
import time
import json
import netifaces

# import the generated classes
import game_chief.gamechief_pb2 as gamechief_pb2
import game_chief.gamechief_pb2_grpc as gamechief_pb2_grpc


class GameChiefClient(object):
    def __init__(self, params_file_path):
        with open(params_file_path, 'r') as file:
            json_string = file.read()
            parameters = json.loads(json_string)
            self._game_chief_ip = parameters["gameChief"]["ip"]
            self._game_chief_port = parameters["gameChief"]["port"]
            self._interface_name = parameters["gameChief"]["interfaceName"]
   
        self.channel = grpc.insecure_channel(
                        '{}:{}'.format(self._game_chief_ip, self._game_chief_port))

        self.stub = gamechief_pb2_grpc.GameChiefStub(self.channel)


    def _get_remote_port_and_setup(self):
        remote_game_ip = netifaces.ifaddresses(self._interface_name)[netifaces.AF_INET][0]['addr']
        request_param = gamechief_pb2.RemoteGameIP()
        request_param.ipAddress = remote_game_ip
        print("Trying to send ip {}".format(remote_game_ip))
        response = self.stub.GetPortAndGameSetup(request_param)
        return response.port, response.setup
    
    
    def wait_for_port_and_setup(self):
        remote_port = None
        game_setup_json = None

        while remote_port is None and game_setup_json is None:
            try:
                remote_port, game_setup_json = self._get_remote_port_and_setup()
                print("Got remote port {}".format(remote_port))
            except:
                print("Waiting for server ...")
                time.sleep(2)
        
        game_setup = json.loads(game_setup_json)
        return remote_port, game_setup

