import grpc
from concurrent import futures
import time
import json
import threading

from robot_controller.robot_controller import RobotController

# import the generated classes
import game_communication.gamecommunication_single_pb2 as gcs_pb2
import game_communication.gamecommunication_single_pb2_grpc as gcs_pb2_grpc


class GameCommunicationServicer(gcs_pb2_grpc.GameCommunicationServicer):
    """
    gRPC server for Robot Communication Service
    """
    def __init__(self, server_port, params_file_path, game_setup):
        self.game = RobotController(params_file_path, game_setup)
        self._server_port = server_port
        with open(params_file_path, 'r') as file:
            json_string = file.read()
            parameters = json.loads(json_string)
            self._max_workers= parameters["gameCommunicationParams"]["maxWorkers"]


    def _start_server(self):
        game_communication_server = grpc.server(
            futures.ThreadPoolExecutor(max_workers=self._max_workers))
        gcs_pb2_grpc.add_GameCommunicationServicer_to_server(
            self, game_communication_server)
            
        game_communication_server.add_insecure_port('[::]:{}'.format(self._server_port))
        game_communication_server.start()
        print ('Game communication Server running ...')
        self._running = True
        try:
            while self._running:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            game_communication_server.stop(0)
            print('Game communication Server Stopped ...')
        print ('Game communication Server stopping ...')
        game_communication_server.stop(0)


    def start_server(self):
        threading.Thread(target=self._start_server, args=()).start()


    def GetObservationVector(self, request, context):
        agent_observations = self.game.get_observations()
        obs_vec = gcs_pb2.ObservationVector()
        obs_vec.observations.extend(agent_observations)
        return obs_vec


    def GetObservationVectorLength(self, request, context):
        number_of_observations = self.game.number_of_observations()
        response = gcs_pb2.ObservationVectorLength()
        response.observationVectorLength = number_of_observations
        return response


    def MakeAction(self, request, context):
        action = request.action
        reward = self.game.do_action(action)
        response = gcs_pb2.Reward()
        response.reward = reward
        return response


    def IsEpisodeFinished(self, request, context):
        episode_finished = self.game.is_episode_finished()
        response = gcs_pb2.EpisodeFinished()
        response.episodeFinished = episode_finished
        return response


    def CloseGame(self, request, context):
        self.game.close_game()
        response = gcs_pb2.Done()
        response.done = True
        return response


    def ResetGame(self, request, context):
        level_parameters = request.gameParamsJson
        self.game.reset_game(level_parameters)
        numberOfActions = self.game.get_number_of_actions()
        numberOfAgents = self.game.get_number_of_agents()
        return gcs_pb2.GameSetup(numberOfActions=numberOfActions, numberOfAgents=numberOfAgents)
