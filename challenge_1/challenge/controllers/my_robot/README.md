# RobotUprising Artificial invaders 2019 contest
This repo works at the moment for the **challenge_1**

This repo is meant to be used together with [this repo](https://gitlab.com/jsalli/tf-agent-webots) which performs the training of the robot.

## Install Python environment
This repo has been tested with Python 3.6.8 and Webots R2019b. Make sure your system has the following Python packages:
- grpcio
- grpcio-tools
- shapely

Most likely these packages are already installed if you have setup the tf-agents training program from [the repo already mentioned above](https://gitlab.com/jsalli/tf-agent-webots).

**Note for Windows:** Download Shapely-package which has it's geos-dependency built in from link below. Choose the one which matches your system from [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely). Install with `pip install Shapely-1.6.4.post2-cp37-cp37m-win_amd64.whl` or what ever is your system's package name

## gRPC and Protocol buffers
There are two gRPC connections this repo uses to communicate with the tf-agents trainer program.
1. gamecommunication_single.proto
    - Game step/reset/observations and other communication with tf-agents trainer
2. gamechief.proto
    - Get the port from tf-agents trainer to use for game communication

These `.proto` files have to be the same that are used in the tf-agents trainer.

### Building the protocol buffers
Run the following lines in folder "artificialinvaders2019/challenge_1/challenge/controllers/my_robot"
```
python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./game_communication/gamecommunication_single.proto
```
```
python -m grpc_tools.protoc --proto_path=. --python_out=. --grpc_python_out=. ./game_chief/gamechief.proto
```

`gameChief`-object inside `game-params.json` is used to tell Webots the gameChief-servers ip address, port and the interface-name used to connect to that server. Also edit the `ballFilePath`-field to point to the ball's file which is loaded to the scene.

The tf-agents trainer starts needed number of instances of this Webots project when training starts.

## Game levels
[This folder](https://gitlab.com/jsalli/artificialinvaders2019/tree/master/challenge_1/challenge/protos) has the prefabs of the different levels. When the agent makes progress and a new level is loaded, the new items are in here and in the sub folders. The `.json`-files inside the level_x-folders contain the points which make the lines of the object. These lines are used in detecting walls and goals. The `startArea` inside `arena.wbo.json` contains the corner points of the area the balls and robot can be placed on that level's arena.