import math
import json

from invader_bot import InvaderBot
from robot_controller.utils import check_object_observation, check_walls_and_goals
from robot_controller.world import World


NUMBER_OF_ACTIONS = 5
NUMBER_OF_AGENTS = 1

class RobotController():
    def __init__(self, params_file_path, game_setup):
        print("RobotController init")
        self._bot = InvaderBot()
        self._bot.setup()
        self._robot_element = self._bot.robot.getSelf()
        self._angles = game_setup["angles"]
        self._ray_length = game_setup["rayLength"]
        self._turn_speed = game_setup["turnSpeed"]
        self._robot_speed = game_setup["robotSpeed"]
        self._action_length = game_setup["actionLength"]
        self._initialized = False

        game_parameters = None
        with open(params_file_path, 'r') as file:
            json_string = file.read()
            game_parameters = json.loads(json_string)

        self._world = World(self._bot, game_parameters["prefabPath"], game_parameters["arenaParams"])


    def get_observations(self):
        local_origo, local_angles = self._get_local_position_and_angles(self._robot_element, self._angles[:])
        object_coordinates = [self._bot.get_balls()] + [[[]]] + [[[]]] # The empty arrays are green balls and enemy robot location
        print("#####object_coordinates")
        print(object_coordinates)
        print("#####object_coordinates")
        ball_obs = check_object_observation(local_origo, local_angles, self._ray_length, object_coordinates)
        wall_obs = check_walls_and_goals(local_origo, local_angles, self._ray_length, self._walls_and_goals_lines)
        
        return [*ball_obs, *wall_obs]


    def _get_local_position_and_angles(self, bot, angles):
        position = bot.getField("translation")
        local_origo = (position.getSFVec3f()[0], position.getSFVec3f()[2])

        local_rotation = bot.getOrientation()
        local_rotation = math.atan2(local_rotation[0], local_rotation[2])
        local_rotation = math.degrees(local_rotation) -90

        local_angles = angles
        for i in range(len(local_angles)):
            local_angles[i] = local_angles[i] - local_rotation + 90

        return local_origo, local_angles

    # For debuging
    # def get_ball_observation(self):
    #     local_origo, local_angles = self._get_local_position_and_angles(self._robot_element, self._angles[:])
    #     object_coordinates = [self._bot.get_balls()] + [[[]]] + [[[]]] # The empty arrays are green balls and enemy robot location
    #     return check_object_observation(local_origo, local_angles, self._ray_length, object_coordinates)


    # For debuging
    # def get_other_observation(self):
    #     local_origo, local_angles = self._get_local_position_and_angles(self._robot_element, self._angles[:])
    #     return check_walls_and_goals(local_origo, local_angles, self._ray_length, self._walls_and_goals_lines)


    def do_action(self, action):
        self._world.continue_simulation()
        self._move(action)
        self._world.pause_simulation()
        self._current_step += 1
        return self._calculate_reward()


    def is_episode_finished(self):
        result = self._episode_finished
        if result is False:
            if self._current_step > self._max_steps -1:
                result = True
        return result

    
    def reset_game(self, level_parameters=""):
        if self._initialized is False and level_parameters is "":
            raise Exception("First reset call should have level_parameters")
        
        new_level_params = None
        self._current_step = 0
        self._episode_finished = False
        if level_parameters is not "":
            self._initialized = True
            level_setup = json.loads(level_parameters)
            self._max_steps = level_setup["agentMaxSteps"]
            new_level_params = level_setup
        
        self._world.pause_simulation()
        self._world.reset_world(new_level_params)
        self._world.continue_simulation()

        if level_parameters is not "":
            self._walls_and_goals_lines = self._world.generate_wall_and_goal_lines()


    def number_of_observations(self):
        return len(self.get_observations())


    def close_game(self):
        self._world.close_program()


    def get_number_of_actions(self):
        return NUMBER_OF_ACTIONS

    def get_number_of_agents(self):
        return NUMBER_OF_AGENTS


    def _set_motors(self, l_motor, r_motor):
        if l_motor is not None and r_motor is not None:
            self._bot.set_motors(l_motor, r_motor)


    def _move(self, action):
        l_motor = None
        r_motor = None

        # Forward
        if action == 1:
            l_motor = self._robot_speed
            r_motor = self._robot_speed
        # Backward
        elif action == 2:
            l_motor = -self._robot_speed
            r_motor = -self._robot_speed
        # Turn Clockwise
        elif action == 3:
            l_motor = self._robot_speed * self._turn_speed
            r_motor = -self._robot_speed * self._turn_speed
        # Turn Anti Clockwise
        elif action == 4:
            l_motor = -self._robot_speed * self._turn_speed
            r_motor = self._robot_speed * self._turn_speed
        # No action
        elif action == 0:
            pass
        else:
            raise Exception("Unknown action {}".format(action))
        
        self._set_motors(l_motor, r_motor)

        step = 0 
        while self._bot.step():
            step += 1
            if step > self._action_length - 1:
                break
        self._bot.step()
    

    def _calculate_reward(self):
        reward = -1.0 / self._max_steps
        blue_goal_score, red_goal_score = self._world.count_new_goals()
        if red_goal_score > 0:
            reward = -1.0
            self._episode_finished = True
            # print("==== BALL IN WRONG. REWARD -1 ====")
        if blue_goal_score > 0:
            reward = 1.0
            self._episode_finished = True
            # print("==== BALL IN RIGHT. REWARD +1 ====")
        return reward
