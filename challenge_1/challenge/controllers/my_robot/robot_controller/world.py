import math
import random
import json
import shapely
from shapely.geometry import Point, Polygon

from robot_controller.utils import create_object_lines, create_line_end_points


ROBOT_START_ANGLE_MIN = 0
ROBOT_START_ANGLE_MAX = 2 * math.pi

SIMULATION_MODE_PAUSE = 0
SIMULATION_MODE_REAL_TIME = 1
SIMULATION_MODE_RUN = 2
SIMULATION_MODE_FAST = 3

GOALS_GROUP_NAME = "_goals"
CORNER_BLOCKS_GROUP_NAME = "_corner_blocks"
ARENA_GROUP_NAME = "_ground"

BLUE_GOAL_GROUP_NAME = "_blue_goal"
RED_GOAL_GROUP_NAME = "_red_goal"
MY_ROBOT_GROUP_NAME = "my_robot"
GOALIE_GROUP_NAME = "_goalie"

BALLS_GROUP_NAME = "_balls"

GOAL_LENGTH = 16
GOAL_PRECISION = 0.1

CLEAR_OFF_DISTANCE = 0.4

class World():
    def __init__(self, bot, prefab_path, arenaParams):
        self._bot = bot
        self._supervisor = self._bot.robot
        self._prefab_path = prefab_path
        self._arenaParams = arenaParams
        self._start_area = None
        self._current_level = -1
        self._current_blue_balls = -1
        self._current_red_balls = -1


    def reset_world(self, level_setup):
        if level_setup is not None:
            arenaNumber = level_setup["arenaNumber"]
            self._yellow_balls_number = level_setup["yellowBallsNumber"]
            self._green_balls_number = level_setup["greenBallsNumber"]
            self._goalie_in_game = level_setup["goalieInGame"]

            if self._current_level is not arenaNumber or self._current_level is -1:
                self._current_level = arenaNumber
                self._load_new_arena(arenaNumber, self._goalie_in_game)
                path = self._prefab_path + self._arena_path
                path = path.replace("/arena.wbo", "")
                arena_params = self._load_json_file(path + "/arena.wbo.json")
                self._start_area = self._generate_new_start_area(arena_params["startArea"]["points"])

        self._set_random_start_positions()


    def count_new_goals(self):
        yellow_balls_root = self._supervisor.getFromDef(BALLS_GROUP_NAME)
        yellow_balls_root = yellow_balls_root.getField("children")
        yellow_balls_in_blue_goal, yellow_balls_in_red_goal = self._loop_balls(yellow_balls_root)

        # green_balls_root = self._supervisor.getFromDef(GREEN_BALLS_GROUP_NAME)
        # green_balls_root = green_balls_root.getField("children")
        # green_balls_in_blue_goal, green_balls_in_red_goal = self._loop_balls(green_balls_root)

        return yellow_balls_in_blue_goal, yellow_balls_in_red_goal #, green_balls_in_blue_goal, green_balls_in_red_goal


    def close_program(self):
        self._supervisor.simulationQuit(0)
    

    def continue_simulation(self):
        self._supervisor.simulationSetMode(SIMULATION_MODE_RUN)


    def pause_simulation(self):
        self._supervisor.simulationSetMode(SIMULATION_MODE_PAUSE)

    
    def generate_wall_and_goal_lines(self):
        path = self._prefab_path + self._arena_path
        path = path.replace("/arena.wbo", "")
        arena_params = self._load_json_file(path + "/arena.wbo.json")
        corner_blocks_params = self._load_json_file(path + "/corner_blocks.wbo.json")
        goals_params = self._load_json_file(path + "/goals.wbo.json")

        arena_line_end_points = create_line_end_points(arena_params["walls"]["points"])
        corner_blocks_end_points_1 = create_line_end_points(corner_blocks_params["1"]["points"])
        corner_blocks_end_points_2 = create_line_end_points(corner_blocks_params["2"]["points"])
        wall_line_points = arena_line_end_points + corner_blocks_end_points_1 + corner_blocks_end_points_2
        blue_goal_end_points = create_line_end_points(goals_params["blueGoal"]["points"])
        red_goal_end_points = create_line_end_points(goals_params["redGoal"]["points"])
        lines_per_type = create_object_lines([wall_line_points] + [blue_goal_end_points] + [red_goal_end_points])
        return lines_per_type

    def _set_random_start_positions(self):
        number_of_objects = self._yellow_balls_number + self._green_balls_number + 1 #The last one is our robot
        print("### number_of_objects) {}".format(number_of_objects))

        random_start_points = self._generate_random_start_positions(
            number_of_objects, self._start_area, CLEAR_OFF_DISTANCE)

        print("len(random_start_points): {}".format(len(random_start_points)))
        self._set_robot_start_position_and_rotations(
            random_start_points.pop(),
            random.uniform(ROBOT_START_ANGLE_MIN, ROBOT_START_ANGLE_MAX))
 
        print("len(random_start_points) after bot: {}".format(len(random_start_points)))

        self._set_ball_positions(BALLS_GROUP_NAME, self._yellow_balls_number, random_start_points[:self._yellow_balls_number], self._yellow_ball_prefab)
        # self._set_ball_positions(BALLS_GROUP_NAME, self._green_balls_number, random_start_points[self._yellow_balls_number:], self._green_ball_prefab)


    def _set_ball_positions(self, ball_group_name, number_of_balls, random_positions, ball_prefab):
        if number_of_balls != len(random_positions):
            raise Exception("Number of wanted balls ({}) does not match with number of given random positions ({})".format(number_of_balls, len(random_positions)))

        balls_root = self._bot.robot.getFromDef(ball_group_name).getField("children")
        while balls_root.getCount() < number_of_balls:
            balls_root.importMFNode(0, ball_prefab)

        for i in reversed(range(balls_root.getCount())):
            ball = balls_root.getMFNode(i)
            ball_position_field = ball.getField("translation")
            ball.setVelocity([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            ball_position_field.setSFVec3f([random_positions[i].x, 0.005, random_positions[i].y])
        

    def _set_robot_start_position_and_rotations(self, random_pos, random_rot):
        self._bot.set_motors(0, 0)
        robot_element = self._bot.robot.getSelf()
        robot_position_field = robot_element.getField("translation")
        robot_position_field.setSFVec3f([random_pos.x, 0.005, random_pos.y])
        robot_rotation_field = robot_element.getField("rotation")
        robot_rotation_field.setSFRotation([0, -1, 0, random_rot])


    def _generate_random_start_positions(self, number_of_objects, start_area, clear_off_distance):
        MAX_TRIES = 500
        random_start_points = []
        minx, miny, maxx, maxy = start_area.bounds
        counter = 0
        step = 0
        while counter < number_of_objects and step < MAX_TRIES:
            new_point = Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
            if start_area.contains(new_point):
                if len(random_start_points) == 0:
                    random_start_points.append(new_point)
                    counter += 1
                else:
                    point_clear = True
                    for point in random_start_points:
                        if point.distance(new_point) < clear_off_distance:
                            point_clear = False
                            break
                    if point_clear == True:
                        random_start_points.append(new_point)
                        counter += 1

            step += 1
        
        if step == MAX_TRIES:
            raise Exception("Could not find start places for all objects")
        
        print("_generate_random_start_positions: {}".format(len(random_start_points)))
        return random_start_points


    def _generate_new_start_area(self, start_area_points):
        return Polygon(start_area_points)


    def _load_new_arena(self, level_number, goalie_in_game):
        self._remove_old_objects()
        root_node = self._supervisor.getRoot().getField("children")

        self._arena_path = self._arenaParams["arenaPrefab"][level_number]

        self._load_prefab(root_node, self._arenaParams["arenaPrefab"][level_number])
        self._load_prefab(root_node, self._arenaParams["cornerBlocksPrefab"][level_number])
        self._load_prefab(root_node, self._arenaParams["goalsPrefab"][level_number])
        if goalie_in_game is True:
            self._load_prefab(root_node, self._arenaParams["goaliePrefab"][level_number])

        self._yellow_ball_prefab = self._prefab_path + self._arenaParams["yellowBallPrefab"][level_number]
        self._green_ball_prefab = self._prefab_path + self._arenaParams["greenBallPrefab"][level_number]

        self._blue_goal = self._supervisor.getFromDef(BLUE_GOAL_GROUP_NAME)
        self._red_goal = self._supervisor.getFromDef(RED_GOAL_GROUP_NAME)


    def _load_prefab(self, parent, new_child_file):
        parent.importMFNode(0, self._prefab_path + new_child_file)


    def _remove_balls(self):
        pass


    def _remove_old_objects(self):
        goals_node = self._supervisor.getFromDef(GOALS_GROUP_NAME)
        corner_block_nodes = self._supervisor.getFromDef(CORNER_BLOCKS_GROUP_NAME)
        arena_node = self._supervisor.getFromDef(ARENA_GROUP_NAME)
        goalie_node = self._supervisor.getFromDef(GOALIE_GROUP_NAME)
        
        self._remove_object(goalie_node) # For some reason this has to be deleted before corner_block_nodes. Other wise error
        self._remove_object(goals_node)
        self._remove_object(arena_node) # For some reason arena_node has to be deleted before corner_block_nodes. Other wise error
        self._remove_object(corner_block_nodes)


    def _remove_object(self, object_to_delete):
        object_to_delete.remove()


    def _loop_balls(self, ball_root):
        ballCount = ball_root.getCount()
        balls_in_blue_goal = 0
        balls_in_red_goal = 0
        for i in range(ballCount - 1, -1, -1):
            ball = ball_root.getMFNode(i)
            if self._check_goal(ball, self._blue_goal):
                ball_root.removeMF(i)
                balls_in_blue_goal += 1
            elif self._check_goal(ball, self._red_goal):
                ball_root.removeMF(i)
                balls_in_red_goal += 1
        
        return balls_in_blue_goal, balls_in_red_goal


    def _check_goal(self, ball, goal):
        ball_pos = ball.getPosition()
        goal_pos = goal.getPosition()
        goal_angle = goal.getField("rotation").getSFRotation()[
            3]
        goal_width = GOAL_LENGTH  # meter
        goal_precision = GOAL_PRECISION  # meter

        # pole vector
        pole_x = math.cos(goal_angle)*goal_width/2
        pole_z = math.sin(goal_angle)*goal_width/2

        # distance between a point (a ball) to a line segment (a goal)
        dot_product = (ball_pos[0]-goal_pos[0]) * \
            pole_x + (ball_pos[2]-goal_pos[2]) * pole_z
        scale = max(-1, min(1, dot_product / (pole_z**2 + pole_x**2)))
        proj_x = goal_pos[0] + scale * pole_x
        proj_z = goal_pos[2] + scale * pole_z
        distance2 = (proj_x - ball_pos[0])**2 + (proj_z - ball_pos[2])**2

        is_goal = distance2 < goal_precision**2
        return is_goal

    
    def _load_json_file(self, file_path):
        with open(file_path, 'r') as file:
            json_string = file.read()
            return json.loads(json_string)
        return None