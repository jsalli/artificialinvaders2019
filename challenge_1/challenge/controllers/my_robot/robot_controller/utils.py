import shapely
from shapely.geometry import LineString, Point, Polygon
from math import cos  
from math import sin  
from math import pi 


# [ [x0, y0], [x1, y1], [x2, y2]]
# ->
# [ [[x0, y0], [x1, y1]], [[x1, y1], [x2, y2]], [[x2, y2], [x0, y0]]]
def create_line_end_points(array_of_points):
    line_end_points = []
    if len(array_of_points) == 2:
        return [array_of_points]
      
    for i in range(len(array_of_points)):
        start_point = array_of_points[i]
        end_point = None
        if i is (len(array_of_points) - 1):
            end_point = array_of_points[0]
        else:
            end_point = array_of_points[i + 1]

        line_points = [start_point, end_point]
        line_end_points.append(line_points)

    return line_end_points


def create_object_lines(line_end_points):
    all_object_lines = []
    for line_coords_per_type in line_end_points:
        lines_of_one_type = []
        for line_coords in line_coords_per_type:
            # print("X: {}, Y: {}".format(line_coords[0][0], line_coords[0][1]))
            start_point = Point(line_coords[0][0], line_coords[0][1])
            # print("X: {}, Y: {}".format(line_coords[1][0], line_coords[1][1]))
            end_point = Point(line_coords[1][0], line_coords[1][1])
            new_line = LineString([start_point, end_point])
            lines_of_one_type.append(new_line)
        all_object_lines.append(lines_of_one_type)
    
    return all_object_lines


def _create_line(coordinates, angle, dist):
  angle = angle * pi / 180.0  
  line = LineString([(coordinates[0], coordinates[1]), (coordinates[0] + dist * sin(angle), coordinates[1] + dist * cos(angle))])
  return line


def _create_sector_vectors(local_origo, angles, ray_length):
    sector_vectors = []
    for angle in angles:
        sector_vector = _create_line(local_origo.coords[0], angle, ray_length)
        sector_vectors.append(sector_vector)
    
    return sector_vectors


def _create_sectors(local_origo, angles, ray_length):
  sector_vectors = _create_sector_vectors(local_origo, angles, ray_length)
  sector_points_array = []
  sectors = []

  for i in range(len(sector_vectors) - 1):
    sector_points_array.append([local_origo.coords[0], sector_vectors[i].coords[1], sector_vectors[i + 1].coords[1]])

  for sector_points in sector_points_array:
    sectors.append(Polygon(sector_points))
  
  return sectors


def check_object_observation(local_origo, angles, ray_length, object_coordinates):
        '''
        local_origo: a Point object of the sectors origo
        angles: Sector angles
        ray_length: Length of the sectors
        object_coordinates: List of lists. Each list contains object coordinates for one type of objects
        '''
        print("object_coordinates")
        print(object_coordinates)
        perceptions = []
        local_origo = Point(local_origo[0], local_origo[1])
        sectors = _create_sectors(local_origo, angles, ray_length)

        for sector in sectors:
            sector_perceptions = [0] * (len(object_coordinates) + 2)
            sector_perceptions[len(sector_perceptions) - 2] = 1.0

            activeGroupIndex = 0
            for coordinates_list in object_coordinates:
                for coordinates in coordinates_list:
                    if len(coordinates) == 0:
                        break
                    object_center = Point(coordinates[0], coordinates[1])
                    in_sector = sector.contains(object_center)
                    if in_sector is True:
                        newDist = local_origo.distance(object_center) / ray_length
                        if newDist < sector_perceptions[len(sector_perceptions) - 1] or sector_perceptions[len(sector_perceptions) - 2] > 0:
                            for i in range(len(object_coordinates)):
                                sector_perceptions[i] = 0.0
                            sector_perceptions[activeGroupIndex] = 1.0
                            sector_perceptions[len(sector_perceptions) - 2] = 0.0
                            sector_perceptions[len(sector_perceptions) - 1] = newDist
                activeGroupIndex += 1
            perceptions.extend(sector_perceptions)
        return perceptions


def check_walls_and_goals(local_origo, angles, ray_length, object_lines):
    perceptions = []
    local_origo = Point(local_origo[0], local_origo[1])
    sector_vectors = _create_sector_vectors(local_origo, angles, ray_length)
    # lines_per_type = create_object_lines(object_line_coordinates)

    for sector_vector in sector_vectors:
            sector_perceptions = [0] * (len(object_lines) + 2)
            sector_perceptions[len(sector_perceptions) - 2] = 1.0

            activeGroupIndex = 0
            for lines_of_one_type in object_lines:
                for line in lines_of_one_type:
                    intersects = line.intersects(sector_vector)
                    if intersects is True:
                        intersect_point = line.intersection(sector_vector)
                        newDist = intersect_point.distance(local_origo) / ray_length
                        if newDist < sector_perceptions[len(sector_perceptions) - 1] or sector_perceptions[len(sector_perceptions) - 2] > 0:
                            for i in range(len(object_lines)):
                                sector_perceptions[i] = 0.0
                            sector_perceptions[activeGroupIndex] = 1.0
                            sector_perceptions[len(sector_perceptions) - 2] = 0.0
                            sector_perceptions[len(sector_perceptions) - 1] = newDist
                activeGroupIndex += 1
            perceptions.extend(sector_perceptions)
    return perceptions