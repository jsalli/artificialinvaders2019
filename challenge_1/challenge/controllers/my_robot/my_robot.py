# from game_communication.game_communication_server import GameCommunicationServicer
# from game_chief.game_chief_client import GameChiefClient

# PARAMETER_FILE_PATH = "game-params.json"

# print("Getting port to use from Game Chief")
# client = GameChiefClient(PARAMETER_FILE_PATH)
# port_to_use, game_setup = client.wait_for_port_and_setup()

# print("Starting Robot communication servicer")
# game_com_server = GameCommunicationServicer(port_to_use, PARAMETER_FILE_PATH, game_setup)
# game_com_server.start_server()


### ======================
### BELOW CODE FOR TESTING
### ======================
from robot_controller.robot_controller import RobotController
import json

PARAMETER_FILE_PATH = "game-params.json"
game_setup = {
    "actionLength" : 1,
    "robotSpeed" : 0.5,
    "turnSpeed" : 0.2,
    "angles" : [-90, -45, -20, 0, 20, 45, 90],
    "rayLength" : 1
}

level_parameters = {
    "agentMaxSteps" : 175.0,
    "arenaNumber" : 2,
    "yellowBallsNumber" : 3,
    "greenBallsNumber" : 2,
    "goalieInGame" : True
}

cont = RobotController(PARAMETER_FILE_PATH, game_setup)
bot = cont._bot
cont.reset_game(json.dumps(level_parameters))
print("Starting")
while True:
    while bot.step():
        all_obs = cont.get_observations()
        print("===")
        print(all_obs)

        # ball_obs = cont.get_ball_observation()
        # ball_obs[::3] = [ '%.0f' % elem for elem in ball_obs[::3]]
        # ball_obs[1::3] = [ '%.0f' % elem for elem in ball_obs[1::3]]
        # ball_obs[2::3] = [ '%.2f' % elem for elem in ball_obs[2::3]]
        # print("===")
        # print("==== Sector 0: {} | 90-45 right".format(ball_obs[0:3]))
        # print("==== Sector 1: {} | 45-20 right".format(ball_obs[3:6]))
        # print("==== Sector 2: {} | 20-0 right".format(ball_obs[6:9]))
        # print("==== Sector 3: {} | 20-0 left".format(ball_obs[9:12]))
        # print("==== Sector 4: {} | 45-20 left".format(ball_obs[12:15]))
        # print("==== Sector 5: {} | 90-45 left".format(ball_obs[15:18]))

        # other_obs = cont.get_other_observation()
        # other_obs[::5] = [ '%.0f' % elem for elem in other_obs[::5]]
        # other_obs[1::5] = [ '%.0f' % elem for elem in other_obs[1::5]]
        # other_obs[2::5] = [ '%.0f' % elem for elem in other_obs[2::5]]
        # other_obs[3::5] = [ '%.0f' % elem for elem in other_obs[3::5]]
        # other_obs[4::5] = [ '%.2f' % elem for elem in other_obs[4::5]]
        # print("===")
        # print("==== Sector 0: {} | 90 right".format(other_obs[0:5]))
        # print("==== Sector 1: {} | 45 right".format(other_obs[5:10]))
        # print("==== Sector 2: {} | 20 right".format(other_obs[10:15]))
        # print("==== Sector 3: {} | forward".format(other_obs[15:20]))
        # print("==== Sector 4: {} | 20 left".format(other_obs[20:25]))
        # print("==== Sector 5: {} | 45 left".format(other_obs[25:30]))
        # print("==== Sector 6: {} | 90 left".format(other_obs[30:35]))